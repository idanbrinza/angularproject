import { Weather } from './interface/weather';
import { WeatherRaw } from './interface/weather-raw';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "ea3aed363be760bc8bb76622ec09d913";
  private IMP = "&units=metric";

  userCollection:AngularFirestoreCollection = this.database.collection('users');
  cityCollection:AngularFirestoreCollection;

 searchWeatherData(cityName:string): Observable<Weather> {
   return this._http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
      .pipe(
        map(data => this.transformWeatherData(data)),
        catchError(this.handleError)
      )
 }
  
addCity(userId:string , city:string, temp:string){
    console.log(userId);
    console.log('Saving City');
    const cityToSave = {city:city,temperature:temp};
    console.log(cityToSave);
    this.userCollection.doc(userId).collection('cities').add(cityToSave);
}


getCollection(userId): Observable<any[]> {
  this.cityCollection = this.database.collection(`users/${userId}/cities`);
  console.log('city collection created');
  return this.cityCollection.snapshotChanges().pipe(
    map(actions => actions.map(a => {
      const data = a.payload.doc.data();
      data.id = a.payload.doc.id;
      return { ...data };
    }))
  );
}


deleteCity(userId:string, id:string){
  this.database.doc(`users/${userId}/cities/${id}`).delete();
}
/*
searchWeatherData(cityName:string): Observable<Weather> {
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
                    .pipe(
                      map(data => this.transformWeatherData(data)),
                      catchError(this.handleError),
                      tap(data =>console.log(JSON.stringify(data))),

                    )  
 }
*/

  constructor(private _http: HttpClient, private database:AngularFirestore) { }
  

 private handleError(res: HttpErrorResponse) {
    console.error(res.error);
    return throwError(res.error || 'Server error');
}

  private transformWeatherData(data:WeatherRaw):Weather {
    return {
      name:data.name,
      country:data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature: data.main.temp,
      lat: data.coord.lat,
      lon: data.coord.lon
    }
  }
}