import { ReviewService } from './../review.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  loadClassified:boolean = false;
  url:string;
  text:string;
  onSubmit(){
    this.loadClassified = false;
    this.reviewService.doc = this.text;
    this.loadClassified = true;
//    this.router.navigate(['/collection']);
  }
  constructor(private reviewService:ReviewService, 
            //public imageService:ImageService,
              private router:Router) { }

  ngOnInit() {  
  }
}
