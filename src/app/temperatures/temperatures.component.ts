import { AuthService } from './../auth.service';
import { Weather } from './../interface/weather';
import { Observable } from 'rxjs';
import { WeatherService } from './../temp.service';
import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {


  constructor(private authService: AuthService, private route: ActivatedRoute,
    private weatherservice: WeatherService) {

  }

  userId: string;
  likes = 0;
  temperature;
  image;
  city;
  tempData$: Observable<Weather>;
  errorMessage: string;
  hasError: boolean = false;
  saveBtn: string = "Save"; 
  isDisabled: boolean=false;
  addLikes() {
    this.likes++
  }


  saveCity() {
    this.weatherservice.addCity(this.userId, this.city, this.temperature);
    this.saveBtn = "Saved!"
    this.isDisabled =true;
  }


  ngOnInit() {
    this.authService.user.subscribe(user => {
      if (user)
        this.userId = user.uid;
    });
    //this.temperature = this.route.snapshot.params.temp;
    this.city = this.route.snapshot.params.city;
    this.tempData$ = this.weatherservice.searchWeatherData(this.city);
    this.tempData$.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.image = data.image;
      },
      error => {
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
    )
  }

}
