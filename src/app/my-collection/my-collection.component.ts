import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { WeatherService } from '../temp.service';
import { AuthService } from '../auth.service';
import { Weather } from '../interface/weather';
import { ReviewService } from '../review.service';

@Component({
  selector: 'app-my-collection',
  templateUrl: './my-collection.component.html',
  styleUrls: ['./my-collection.component.css']
})
export class MyCollectionComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 
  constructor(public reviewService:ReviewService,
              //public imageService:ImageService
              ) {}

  ngOnInit() {
    this.reviewService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.reviewService.categories[res])
        this.category = this.reviewService.categories[res];
      //  console.log(this.imageService.images[res]);
      //  this.categoryImage = this.imageService.images[res];
        console.log(this.reviewService.doc)
      }
    )
  }
}
