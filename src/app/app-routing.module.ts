import { MyCollectionComponent } from './my-collection/my-collection.component';
import { TempformComponent } from './tempform/tempform.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './signup/signup.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReviewComponent } from './review/review.component';

const appRoutes: Routes = [
    { path: 'welcome', component: WelcomeComponent },
    { path: 'signup', component: SignUpComponent},
    { path: 'login', component: LoginComponent},
    { path: 'weather/:city', component: TemperaturesComponent},
    { path: 'temp', component: TempformComponent},
    { path: 'collection', component: MyCollectionComponent},
    { path: 'review', component: ReviewComponent},
    { path: '',
      redirectTo: '/welcome',
      pathMatch: 'full'
    },
  ];

  
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }