import { SearchService } from './../search.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { Popular } from '../interface/popular';

@Component({
  selector: 'app-popular',
  templateUrl: './popular.component.html',
  styleUrls: ['./popular.component.css']
})
export class PopularComponent implements OnInit {
  
  constructor(private router: Router, private route:ActivatedRoute, private searchservice:SearchService, private auth: AuthService) {  
    }

  Movies$: Popular;
  TV$: Popular;

  getPoster(t, h)
  {
    var url = "http://img.omdbapi.com/?apikey=d021f5a8&i="+t+"&h="+h;
    return url
  }


  ngOnInit(): void {
    this.searchservice.getPopularMovies()
    .subscribe(data =>this.Movies$ = data );

    this.searchservice.getPopularTV()
    .subscribe(data =>this.TV$ = data ); 
  }

}
