import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  
  private url = "https://kr12lwc3zg.execute-api.us-east-1.amazonaws.com/beta";
  
  public categories:object = {0: 'Negative', 1: 'Positive'}
  
  public doc:string; 
  
  classify():Observable<any>{
    console.log(this.doc);
    let json = {
      "reviews": [
        {
          "text": this.doc
        },
      ]
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        let final = res.body.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;      
      })
    );      
  }

  constructor(private http: HttpClient) { }
}
