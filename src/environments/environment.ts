// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDqB2UZRRTb5X_Tt0tTQPRBjOFLh394q0I",
    authDomain: "imbd-b9d49.firebaseapp.com",
    databaseURL: "https://imbd-b9d49.firebaseio.com",
    projectId: "imbd-b9d49",
    storageBucket: "imbd-b9d49.appspot.com",
    messagingSenderId: "740926316747",
    appId: "1:740926316747:web:287ac4ee71f006fbc7fb4e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
